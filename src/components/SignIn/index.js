import React, { Component } from "react";
import './styles.scss'
import Button from "../forms/Button";
import {signInWithGoogle, auth} from './../../FireBase/utils';
import FormInput from "../forms/formInput";
import AuthWrapper from "../AuthWrapper";
import { Link } from "react-router-dom";
const intialState = {
    email : '', 
    password : '',
}

class SignIn extends Component
{

    constructor(props){
        super(props)
        this.state = {
            ...intialState
        };
        this.handleChange = this.handleChange.bind(this);
    }

    handleSubmit = async e =>{
        e.preventDefault();
        const {email , password} = this.state;

        try {
            await auth.signInWithEmailAndPassword(email , password); 
            this.setState({
                ...intialState
            })
        } catch (error) {
            console.log(error);
        }
    }

    handleChange(e) {
        const {name , value} = e.target;

        this.setState({
            [name] : value
        })
    }

    render(){
        const {email , password} = this.state;

        const configAuthWrapper = {
            headline : 'LogIn'
        }
        return(

            <AuthWrapper {...configAuthWrapper}>
                <div className="formWrap">
                    <form onSubmit={this.handleSubmit}>
                        <FormInput
                                type="email"
                                name="email"
                                value={email}
                                placeholder="Email"
                                handleChange={this.handleChange}
                        />
                        <FormInput
                                type="password"
                                name="password"
                                value={password}
                                placeholder="password"
                                handleChange={this.handleChange}
                        />
                        <Button type='submit'>
                            LogIn
                        </Button>
                        <div className="socialSignIn">
                            <div className="row">
                                <Button onClick={signInWithGoogle}>
                                    SignIn With Google
                                </Button>
                            </div>
                        </div>
                        <div className="links">
                            <Link to='/recovery'>
                                Reset Password
                            </Link>
                        </div>
                    </form>
                </div>
            </AuthWrapper>
        )
    }

}
    
export default SignIn;