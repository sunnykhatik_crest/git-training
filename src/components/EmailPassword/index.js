import React, { Component } from "react";
import AuthWrapper from "../AuthWrapper";
import FormInput from "../forms/formInput";
import Button from "../forms/Button";
import './styles.scss'
import { auth } from "../../FireBase/utils";
import {withRouter} from '../../../src/withRouter';
const initialState = {
    email : '',
    errors : []
}

class EmailPassword extends Component{
    constructor(props){
        super(props)
        this.state = {
            ...initialState
        }
        this.handleChange= this.handleChange.bind(this)
    }
         
    handleChange(e){
        const {name , value} = e.target

        this.setState({
            [name] : value
        })
    }

    handleSubmit = async (e) =>{
        e.preventDefault();

        try {
            const {email} = this.state
            const config = {
                url : 'http://localhost:3000/login'
            }
            await auth.sendPasswordResetEmail(email , config)
            .then(() =>{
                console.log('Password Reset');
                this.props.history.push('/login')
            })
            .catch(()=>{
                console.log('Something Went Wrong');
                const err = ['Email not found try again']
                this.setState({
                    errors : err
                })
            })
        } catch (error) {
            console.log(error);
        }

    }
    render(){


        const {email, errors } = this.state
    const configAuthWrapper = {
        headline : 'Email password'
    }
        return( 
        <AuthWrapper {...configAuthWrapper}>
            <div className="formWrap">
                {errors.length > 0 && (
                    <ul>
                        {errors.map((e, index) => {
                            return(
                                <li key={index}>
                                    {e}
                                </li>
                            )
                        })}
                    </ul>
                )}
                <form onSubmit={this.handleSubmit}>
                    <FormInput
                        type="email"
                        name="email"
                        value={email}
                        placeholder="email"
                        onChange={this.handleChange}
                    />

                    <Button type="submit">
                        Email password
                    </Button>
                </form>
            </div>
        </AuthWrapper>)
    }
}

export default withRouter(EmailPassword);