import React from "react";
import Logo from "./../../assets/logo.png"
import { Link } from "react-router-dom";
import './styles.scss';
import { auth } from "../../FireBase/utils";
import { connect } from "react-redux";
const Header = props => {

    const {currentUser} = props;
    console.log(currentUser)
    return(
        <header className="header">
            <div className="wrap">
                <div className="logo">
                    <Link to="/">
                    <img className="imagde" src={Logo} alt="Simple logo for the application"/>
                    </Link>
                </div>
                <div className="callToActions">
                    {currentUser && (
                        <ul>
                            <li>
                                <span onClick={() => auth.signOut()}>
                                    LogOut
                                </span>
                            </li>
                        </ul>
                    )}
                    {!currentUser && (
                         <ul>
                         <li>
                             <Link to="/registration">
                                 Register
                             </Link>
                         </li>
                         <li>
                             <Link to="/login">
                                 Login
                             </Link>
                         </li>
                     </ul>
                    )}
                   
                </div>
            </div>
        </header>
    )
}
Header.defaultProps = {
    currentUser : null
}
const mapStateToProp = ({user}) => ({
    currentUser: user.currentUser
});
export default connect(mapStateToProp, null)(Header);