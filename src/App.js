import React, { Component } from "react";
import './default.scss'
import HomePage from "./Pages/HomePage"
import Registraion from "./Pages/Registration"
import { Route, Routes, Navigate} from "react-router-dom";
import MainLayout from "./MainLayout/MainLayout";
import HomepageLayout from "./MainLayout/HomepageLayout";
import Login from './Pages/LoginPage';
import Recovery from "./Pages/Recovery";
import {auth, handleUserProfile} from './FireBase/utils';  
import { setCurrentUser } from "./Redux/User/user.actioons";
import { connect } from "react-redux";

class App extends Component{  
  authListner = null;

  componentDidMount(){

    const {setCurrentUser} = this.props
    this.authListner = auth.onAuthStateChanged(async userAuth =>{
      if(!userAuth){
       const userRef = await handleUserProfile(userAuth);
        userRef.onSnapshot( snapshot => {
          setCurrentUser(
            {
              id:snapshot.id,
              ...snapshot.date()
            }
          )
        })

      };
      setCurrentUser(userAuth);
    })

  }

  componentWillUnmount(){
      this.authListner()
  } 

  
  render()
  {
    const {currentUser} = this.props;
    return(
      <div className="App">
        <Routes>
          <Route path='/' element={
          <HomepageLayout>
            <HomePage/>
          </HomepageLayout>} />
          <Route path='/registration' element={
            () => currentUser ? <Navigate to='/'/>
            :(
              <MainLayout>
                <Registraion/>
              </MainLayout>
            )
          }/>
          <Route path='/login' 
          element={
            () => currentUser ?
              <Navigate to='/' replace/> :
            (
              <MainLayout>
                <Login/>
              </MainLayout>
            )}/>
          <Route path="/recovery" element={() => (
            <MainLayout>
              <Recovery/>
            </MainLayout>
          )}/>
        </Routes>
    </div> 
    )
  }
}
const mapStateToProps = ({user}) =>({
  currentUser : user.currentUser
})

const mapDispatchToProps = dispatch => ({
  setCurrentUser: user => dispatch(setCurrentUser(user))
})
export default connect(mapStateToProps , mapDispatchToProps)(App);
