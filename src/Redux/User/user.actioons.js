import userTypes from './user.reducer'

export const setCurrentUser = user => ({
    type : userTypes.SET_CURRENT_USER,
    payload : user
})